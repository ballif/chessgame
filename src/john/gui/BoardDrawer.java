package john.gui;


import john.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 *
 */
public class BoardDrawer implements Observer {


    private static Color white = new Color(255, 255, 255),
            black = new Color(183, 180, 178);

    private static Font font = new Font("Times-New-Roman", Font.PLAIN, 50);
    private static Font smallerFont = new Font("Times-New-Roman", Font.PLAIN, 30);
    private ChessBoard chessBoard;
    private JFrame jFrame;
    private JLabel[][] labelBoard;

    private JTextField textbox;
    private InputTaker inputTaker;


    public BoardDrawer(ChessBoard chessBoard) {
        this.chessBoard = chessBoard;

        inputTaker = new InputTaker(chessBoard);
        // 1 - Chess Board Here
        JPanel boardPanel = new JPanel(new GridLayout(9, 9, 0, 0));
        boardPanel.setBorder(new EmptyBorder(20, 0, 0, 20));
        labelBoard = new JLabel[8][8];

        for (int j = 7; j >= 0; j--) {
            JLabel tag = new JLabel(Integer.toString(j + 1), SwingConstants.CENTER);
            tag.setOpaque(true);
            tag.setFont(smallerFont);
            boardPanel.add(tag);
            for (int i = 0; i < 8; i++) {
                labelBoard[i][j] = new JLabel(pieceUnicode(chessBoard.getAt(i, j)), SwingConstants.CENTER);
                Color background = (i + j) % 2 == 0 ? black : white;
                labelBoard[i][j].setBackground(background);
                labelBoard[i][j].setOpaque(true);
                labelBoard[i][j].setFont(font);
                boardPanel.add(labelBoard[i][j]);
            }
        }

        String letters = " abcdefgh";
        for (char x : letters.toCharArray()) {
            JLabel tag = new JLabel(Character.toString(x), SwingConstants.CENTER);
            tag.setOpaque(true);
            tag.setFont(smallerFont);
            boardPanel.add(tag);
        }

        // 2 - Input Taker Here
        JPanel inout = new JPanel(new GridLayout(1, 1, 5, 5));

        textbox = inputTaker.getLabel();

        inout.add(chessBoard.getWhiteTimer());
        inout.add(textbox);
        inout.add(chessBoard.getMessage());
        inout.add(chessBoard.getBlackTimer());


        jFrame = new JFrame();
        jFrame.setLayout(new BorderLayout());
        jFrame.setAlwaysOnTop(true);
        jFrame.setTitle("Chess Game v1.1 @ blueVector");
        jFrame.add(boardPanel, BorderLayout.CENTER);
        jFrame.add(inout, BorderLayout.SOUTH);
        jFrame.getContentPane().setBackground(new Color(108, 65, 139));
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.pack();
        jFrame.setSize(500, 550);
        jFrame.setVisible(true);

    }

    public void update(Observable e, Object arg) {

        int[] position = (int[]) arg;
        if (position == null)
            return;
        for (int t = 0; t < position.length; t = t + 2) {
            int x0 = position[t];
            int y0 = position[t + 1];
            String text = pieceUnicode(chessBoard.getAt(x0, y0));
            labelBoard[x0][y0].setText(text);
        }
//        jFrame.revalidate();
        textbox.setBackground(chessBoard.getTurn() ? white : black);

        if(chessBoard.isEnd()) {
            // do nothing
        }

    }

    public String pieceUnicode(piece p) {

        return p.getUnicode();
    }

    public void restart() {

        inputTaker = new InputTaker(chessBoard);
        // 1 - Chess Board Here
        JPanel boardPanel = new JPanel(new GridLayout(9, 9, 0, 0));

        boardPanel.setBorder(new EmptyBorder(20, 0, 0, 20));
        labelBoard = new JLabel[8][8];

        for (int j = 7; j >= 0; j--) {
            JLabel tag = new JLabel(Integer.toString(j + 1), SwingConstants.CENTER);
            tag.setOpaque(true);
            tag.setFont(smallerFont);
            boardPanel.add(tag);
            for (int i = 0; i < 8; i++) {
                labelBoard[i][j] = new JLabel(pieceUnicode(chessBoard.getAt(i, j)), SwingConstants.CENTER);
                Color background = (i + j) % 2 == 0 ? black : white;
                labelBoard[i][j].setBackground(background);
                labelBoard[i][j].setOpaque(true);
                labelBoard[i][j].setFont(font);
                boardPanel.add(labelBoard[i][j]);
            }
        }

        String letters = " abcdefgh";
        for (char x : letters.toCharArray()) {
            JLabel tag = new JLabel(Character.toString(x), SwingConstants.CENTER);
            tag.setOpaque(true);
            tag.setFont(smallerFont);
            boardPanel.add(tag);
        }

        // 2 - Input Taker Here
        JPanel inout = new JPanel(new GridLayout(1, 1, 5, 5));

        textbox = inputTaker.getLabel();

        inout.add(chessBoard.getWhiteTimer());
        inout.add(textbox);
        inout.add(chessBoard.getMessage());
        inout.add(chessBoard.getBlackTimer());


        jFrame.getContentPane().invalidate();

        jFrame.setLayout(new BorderLayout());
        jFrame.setAlwaysOnTop(true);
        jFrame.setTitle("Chess Game v1.1 @ blueVector");
        jFrame.add(boardPanel, BorderLayout.CENTER);
        jFrame.add(inout, BorderLayout.SOUTH);
        jFrame.getContentPane().setBackground(new Color(108, 65, 139));
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.pack();
        jFrame.setSize(500, 550);
        jFrame.setVisible(true);

    }

}
