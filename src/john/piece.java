package john;

/**
 * Created by john on 7/7/14.
 */
public interface piece {


    /**
     * Check these :
     * 1- the piece has that move function by its nature
     */
    public boolean checkMove(int startx, int starty, int endx, int endy);
    public boolean isCaptured();

    /**
     * Returns all the moves that can be done by piece, considering all situations:
     * 1 - There is a same color piece next to this piece
     * 2 - this piece is on the border
     *
     * BUT NOT :
     * 1 - If this piece makes these moves King can fall into check/danger
     * So we never use this in place for checkMove() - this will be wrong
     * This can cause HUGE problems.
     * 2 - We are going to use this function to check that if the game ended draw
     * @return
     */
    public int [] allMoves();
    public boolean sameColor(boolean color);
    public String getUnicode();
}
