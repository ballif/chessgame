package john.allPieces;
import john.*;

/**
 * This class implements the King in compliance with the <i>piece</i> interface. Note that it is immutable class,
 * so that there will not be any problem at clone method.
 */
public class King implements piece {

    /** Defines which board the piece is on */
    private final ChessBoard board;

    /** Defines the color of the piece */
    private final boolean isWhite;


    public King(ChessBoard board, boolean isWhite) {

        this.board = board;
        this.isWhite = isWhite;
    }

    /** {@inheritDoc} */
    public boolean checkMove(int startx, int starty, int endx, int endy) {

        if ( Math.abs(endy - starty) <=1 && Math.abs(endx - startx) <= 1 ) {
            return true;
        }

        // Castling for White
        if (isWhite && startx == 4 && starty ==0 ) {
            if (board.isWhiteCastlingRight && endx == 7 && endy == 0)
                return true;
            if (board.isWhiteCastlingLeft && endx == 0 && endy == 0)
                return true;
        }

        // Castling for Black
        if (!isWhite && startx == 4 && starty ==7 ) {
            if(board.isBlackCastlingRight && endx == 7 && endy == 7 )
                return true;
            if(board.isBlackCastlingLeft && endx == 0 && endy == 7)
                return true;
        }

        return false;
    }

    /** {@inheritDoc} */
    public boolean isCaptured() {

        return board.getLocation(this) == null;
    }

    /** {@inheritDoc} */
    public int [] allMoves() {

        return board.allMoves(this);
    }

    /** {@inheritDoc} */
    public boolean sameColor(boolean color) {

        return isWhite==color;
    }

    /** {@inheritDoc} */
    public String getUnicode() {

        return isWhite ? "\u2654" : "\u265A";
    }
}
