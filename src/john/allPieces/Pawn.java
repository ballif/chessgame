package john.allPieces;
import john.*;

/**
 * Created by john on 7/7/14.
 * This class implements the Pawn in compliance with the <i>piece</i> interface. Note that it is immutable class,
 * so that there will not be any problem at clone method.
 */
public class Pawn implements piece {

    /** Defines which board the piece is on */
    private final ChessBoard board;

    /** Defines the color of the piece */
    private final boolean isWhite;


    public Pawn(ChessBoard board, boolean color) {

        this.board = board;
        isWhite = color;
    }

    /** {@inheritDoc} */
    public boolean checkMove(int startx, int starty, int endx, int endy) {

        int direction = isWhite ? 1 : -1; // Color dependent movement direction

        if(starty + direction == endy) { // 1- this is one step ahead regular move
            if(startx == endx)
                return board.getAt(endx, endy) instanceof Empty;

            else if(Math.abs(startx-endx) == 1) { // 2 - capturing move
                piece target = board.getAt(endx, endy);
                if(!target.sameColor(isWhite) && !(target instanceof Empty) )
                    return true;
                if(target instanceof Empty && board.isEnPassant(endx, endy)) //3- enPassant
                    return true;
            }
        }

        if(isWhite && starty == 1 // 4- double step move for white
                && board.getAt(startx, starty + 1) instanceof Empty
                && board.getAt(startx, starty + 2) instanceof Empty
                && startx == endx && endy == starty + 2) {
            board.setEnPassant(endx, endy - direction); // set enPassant flag and location
            return true;
        }

        if(!isWhite && starty == 6 //4- double step for black
                && board.getAt(startx, starty - 1) instanceof Empty
                && board.getAt(startx, starty - 2) instanceof Empty
                && startx == endx && endy == starty - 2) {
            board.setEnPassant(endx, endy - direction); // set enPassant flag and location
            return true;
        }

        return false; // None of the above is applicable
    }

    /** {@inheritDoc} */
    public boolean isCaptured() {

        return board.getLocation(this) == null;
    }


    /** {@inheritDoc} */
    public int [] allMoves() {

       return board.allMoves(this);
    }

    /** {@inheritDoc} */
    public boolean sameColor(boolean color) {

        return isWhite==color;
    }

    /** {@inheritDoc} */
    public String getUnicode() {

        return isWhite ? "\u2659" : "\u265F";
    }

}
