package john.allPieces;

import john.piece;
import john.*;

/**
 * Created by john on 7/7/14.
 * This class implements the Rook in compliance with the <i>piece</i> interface. Note that it is immutable class,
 * so that there will not be any problem at clone method.
 */
public class Rook implements piece {

    /** Defines which board the piece is on */
    private final ChessBoard board;

    /** Defines the color of the piece */
    private final boolean isWhite;


    public Rook(ChessBoard board, boolean color) {

        this.board = board;
        isWhite = color;
    }

    /** {@inheritDoc} */
    public boolean checkMove(int startx, int starty, int endx, int endy) {

        boolean noBlocker = true;
        int direction;

        // check so that we wont have problem in for loop next step
        if(startx == endx && starty == endy)
            return false;

        // check horizontal
        if(starty == endy) {
            direction = endx > startx ? 1 : -1;
            for(int i = startx + direction; i!=endx; i = i+direction)
                noBlocker = noBlocker && board.getAt(i,starty) instanceof Empty;
            return noBlocker;
        }

        // check vertical
        if(startx == endx) {
            direction = endy > starty ? 1 : -1;
            for(int i = starty + direction; i!=endy; i = i + direction)
                noBlocker = noBlocker && board.getAt(startx, i) instanceof Empty;
            return noBlocker;
        }

        return false;
    }

    /** {@inheritDoc} */
    public boolean isCaptured() {

        return board.getLocation(this) == null;
    }


    /** {@inheritDoc} */
    public int [] allMoves() {

        return board.allMoves(this);
    }

    /** {@inheritDoc} */
    public boolean sameColor(boolean color) {

        return isWhite==color;
    }

    /** {@inheritDoc} */
    public String getUnicode() {

        return isWhite ? "\u2656" : "\u265C";
    }


}
