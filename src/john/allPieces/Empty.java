package john.allPieces;
import john.*;
/**
 * Created by john on 7/10/14.
 * This class is used as if null piece in the game. At any time ChessBoard has many of Empty piece, over which the
 * players pieces can move by exchanging their place. Empty piece does not have functional methods, but rather,it has
 * interface required methods.
 */
public class Empty implements piece {

    /** @inheritDoc */
    public Empty(ChessBoard board, boolean color) {}
    public Empty() {}

    /** Irrelevant function defined to yield interface */
    public boolean sameColor(boolean isWhite) {

        return true;
    }

    /** Irrelevant function defined to yield interface */
    public boolean checkMove(int startx, int starty, int endx, int endy) {

        return false;
    }

    /** Irrelevant function defined to yield interface */
    public boolean isCaptured() {

        return true;
    }

    /** Irrelevant function defined to yield interface. Always returns null. */
    public int [] allMoves() {

        return null;
    }

    /** This is only functional method that we can use to print Game screen */
    public String getUnicode() {

        return "";
    }

}
