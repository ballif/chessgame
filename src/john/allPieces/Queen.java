package john.allPieces;

import john.*;
/**
 * Created by john on 7/7/14.
 * This class implements the Queen in compliance with the <i>piece</i> interface. Note that it is immutable class,
 * so that there will not be any problem at clone method.
 */
public class Queen implements piece {

    /** Defines which board the piece is on */
    private final ChessBoard board;

    /** Defines the color of the piece */
    private final boolean isWhite;


    public Queen(ChessBoard board, boolean color) {
        this.board = board;
        isWhite = color;
    }

    /** {@inheritDoc} */
    public boolean checkMove(int startx, int starty, int endx, int endy) {

         return  new Bishop(board, isWhite).checkMove(startx, starty, endx, endy)
            || new Rook(board, isWhite).checkMove(startx, starty, endx, endy);
    }

    /** {@inheritDoc} */
    public boolean isCaptured() {
        return board.getLocation(this) == null;
    }

    /** {@inheritDoc} */
    public int [] allMoves() {

        return board.allMoves(this);
    }

    /** {@inheritDoc} */
    public boolean sameColor(boolean color) {

        return isWhite==color;
    }

    /** {@inheritDoc} */
    public String getUnicode() {

        return isWhite ? "\u2655" : "\u265B";
    }
}
