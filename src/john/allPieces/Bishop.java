package john.allPieces;

import john.*;

/**
 * Created by john on 7/7/14.
 * This class implements the Bishop in compliance with the <i>piece</i> interface. Note that it is immutable class,
 * so that there will not be any problem at clone method.
 */
public class Bishop implements piece {

    /** Defines which board the piece is on */
    private final ChessBoard board;

    /** Defines the color of the piece */
    private final boolean isWhite;



    public Bishop(ChessBoard board, boolean color) {
        this.board = board;
        isWhite = color;
    }

    /** {@inheritDoc} */
    public boolean checkMove(int startx, int starty, int endx, int endy) {

        boolean noBlocker = true;
        int direction;

        if(startx == endx && starty == endy)
            return false;

        if(startx - endx == starty - endy) {
            direction = startx < endx ? 1 : -1;
            for(int i = startx + direction; i!=endx; i = i+direction)
                noBlocker = noBlocker && board.getAt(i, i - startx + starty) instanceof Empty;
            return noBlocker;
        }

        if(endx - startx == starty - endy) {
            direction = startx < endx ? 1 : -1;
            for(int i = startx + direction; i!=endx; i = i+direction)
                noBlocker = noBlocker && board.getAt(i, -i + startx + starty) instanceof Empty;
            return noBlocker;
        }

        return false;
    }

    /** {@inheritDoc} */
    public boolean isCaptured() {

        return board.getLocation(this) == null;
    }

    /** {@inheritDoc} */
    public int [] allMoves() {

        return board.allMoves(this);
    }

    /** {@inheritDoc} */
    public boolean sameColor(boolean color) {

        return isWhite==color;
    }

    /** {@inheritDoc} */
    public String getUnicode() {

        return isWhite ? "\u2657" : "\u265D";
    }
}
