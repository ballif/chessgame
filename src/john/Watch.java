package john;

import javax.swing.*;

/**
 * Created by john on 7/7/14.
 * Implements timing
 */
public class Watch {

    private long lastCheckPoint;
    private JLabel timeLabel;
    private boolean timeIsUp;
    private long remains;
    private boolean counting;
    private String labelText;
    private boolean on;


    public Watch() {

        lastCheckPoint = System.currentTimeMillis();
        timeLabel = new JLabel("30:00", SwingConstants.CENTER);
        timeIsUp = false;
        counting = false;
        remains = 30*60*1000;
        on = true;

    }

    public boolean isEnd(){

        return timeIsUp;
    }

    public boolean update() {

        long temp = System.currentTimeMillis();
        remains =  remains - (counting && on ? ( temp - lastCheckPoint) : 0);
        lastCheckPoint = System.currentTimeMillis();
        timeIsUp = remains <= 0;
        String updatedText = Long.toString(remains / 60000) + "mins, " + Long.toString((remains/1000) % 60) + "secs";
        lastCheckPoint = temp;
        if(updatedText.equals(labelText)) {
            return false;
        } else {
            timeLabel.setText(updatedText);
            labelText = updatedText;
            return true;
        }

    }

    public JLabel getLabel() {

        return timeLabel;
    }

    public void startStop() {

        counting = !counting;
    }

    public void stop() {

        counting = false;
    }

    public void offTimer() {

        on = false;
        timeLabel.setText("");
    }




}
