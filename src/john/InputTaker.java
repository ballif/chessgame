package john;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by john on 7/7/14.
 * In this class we will implement taking new string inputs
 * and checking whether given string makes sense by rules of chess.
 * We will not check game-state dependent move, but rather syntax correctness
 */


 public class InputTaker {


    private JTextField inputLabel;
    private int [] command;
    private boolean ready; // true: there is a command available
    private ChessBoard chessBoard;

    public InputTaker(final ChessBoard chessBoard) {
        this.chessBoard = chessBoard;
        command = new int[4];

        inputLabel = new JTextField(SwingConstants.CENTER);
        inputLabel.setBorder(new EmptyBorder(5,5,5,5));

        inputLabel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chessBoard.setMessage("");
                parseAddCommand(inputLabel.getText());
                inputLabel.setText("");
                Color color = chessBoard.getTurn() ? new Color(255,255,255) : new Color(183, 180, 178);
                inputLabel.setBackground(color);
            }
        });

        setReady(false);
    }


    public void parseAddCommand(String s) {
        if(s.equals("!quit"))
            System.exit(0);
        if(s.equals("!restart"))
            Game.gameStart();
        if(s.equals("!notimer"))
            chessBoard.stopTimers();


        char [] chars = s.toLowerCase().toCharArray();
        if(chars.length == 4) {
            int x0 = chars[0] - 'a';
            int y0 = chars[1] - 49;
            int x1 = chars[2] - 'a';
            int y1 = chars[3] - 49;
            if(x0 > -1 && x0 <8 && y0 > -1 && y0 <8 && x1 > -1 && x1 <8 && y1 > -1 && y1 <8 ) {
                // move is seem to be correct by means of borders
                command = new int []{x0, y0, x1, y1};
                if(chessBoard.checkMove(command, chessBoard.getAt(x0, y0)))
                    chessBoard.move(command, chessBoard.getAt(x0, y0));
                setReady(true);
            }
        }
    }

    public JTextField getLabel() {

        return inputLabel;
    }

    public synchronized void setReady(boolean state) {
        ready = state;
    }



}