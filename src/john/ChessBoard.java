package john;

import john.allPieces.*;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by john on 7/7/14.
 */
public class ChessBoard extends Observable implements Cloneable{

    // 8x8 matrix that holds the piece type if there are any
    private piece [][] board;


    ChessBoard restorePoint;

    public boolean isWhiteCastlingRight;
    public boolean isWhiteCastlingLeft;
    public boolean isBlackCastlingRight;
    public boolean isBlackCastlingLeft;

    private boolean isEnd;
    private boolean isDraw;
    private boolean real;

    private JLabel message;
    /**
     * If true, the playing turn is for white, and black otherwise
     */
    private boolean isWhiteTurn;


    private int [] enPassantable;
    private boolean enPassantColor;

    private Watch whiteWatch;
    private Watch blackWatch;


    /**
     * Initialize the board for the beginning of the game
     */
    public ChessBoard() {

        restart();
    }

    /**
     * moves the given piece to the location on the board
     * @return if the movement is done
     */
    public boolean move(int [] movementVector, piece p) {

        // Use after checking move, not without it

        int x0 = movementVector[0];
        int y0 = movementVector[1];
        int x1 = movementVector[2];
        int y1 = movementVector[3];

        board[x1][y1] = board[x0][y0];
        board[x0][y0] = new Empty();
        int [] changes = movementVector;

        // Implement enpassant capturing here
        if(p instanceof Pawn && enPassantable != null)
            if(enPassantable[0] == x1 && enPassantable[1] == y1) {
                changes = new int [] {x1, y0, x0, y0, x1, y1};
                board[x1][y0] = new Empty();
            }

        if(real) {


            // flip the turn state for other player
            isWhiteTurn = !isWhiteTurn;
            whiteWatch.startStop();
            blackWatch.startStop();

            isDraw(); // draw or win

            // Reset enpassant here for next turn of the same player(check impl for more)
            resetEnPassant();

            //todo implement here for extreme situations such as castling


            setChanged();
            notifyObservers(changes);
        }
        return true;
    }
    public boolean checkMove(int [] movementVector, piece p) {

        int x0 = movementVector[0];
        int y0 = movementVector[1];

        // Check the current position of given piece is correct
        if( !p.getClass().equals(board[x0][y0].getClass())) return false;

        // Check if the color of the piece is players'
        if ( !board[x0][y0].sameColor(isWhiteTurn) ) return false;

        int x1 = movementVector[2];
        int y1 = movementVector[3];

        // Check if the destination is either empty or opposite piece
        if( !(board[x1][y1] instanceof Empty) )
            if( board[x1][y1].sameColor(isWhiteTurn) )
                return false;

        if (!p.checkMove(x0, y0, x1, y1)) return false;

        // Check here if the movement leaves the King checked
        push(); // Now we get to the imaginary path
        move(movementVector, getAt(movementVector[0], movementVector[1]));
        boolean isChecked = controlCheckState(isWhiteTurn);
        pop();

        return !isChecked;
    }
    public void push(){

        restorePoint = this.clone();
        real = false;

    }

    public piece getAt(int x0, int y0){

        return board[x0][y0];
    }

    public int [] getLocation(piece p) {
        for(int i=0; i<8; i++)
            for(int j=0; j<8; j++)
                if(p == board[i][j])
                    return new int [] {i, j};
        return null;
    }

    public boolean controlCheckState(boolean color) {


        // get Location of the King
        int kingX = -1;
        int kingY = -1;
        outerloop:
        for(int i=0; i<8; i++) {
            for(int j=0; j<8; j++) {
                if(board[i][j] instanceof King && board[i][j].sameColor(color)){
                    kingX = i;
                    kingY = j;
                    break outerloop;
                }
            }
        }

        // is King checked
        piece temp;
        for(int i=0; i<8; i++) {
            for(int j=0; j<8; j++) {
                temp = board[i][j];
                if( !(temp instanceof Empty) && temp.sameColor(!color) ) {
                    if (temp.checkMove(i, j, kingX, kingY)) {
//                        setMessage(isWhiteTurn ? "White is checked!" : "Black is checked!");
                        return true;
                    }
                }
            }
        }

        return  false;
    }

    //todo Promotion
    public boolean checkPromotion(){return true;}

    public boolean isDraw(){

        for (int i=0; i<8; i++)
            for(int j=0; j<8; j++)
                if( !(board[i][j] instanceof Empty) && board[i][j].sameColor(isWhiteTurn))
                    if(allMoves(board[i][j]) != null)
                        return false;

        isDraw = !controlCheckState(isWhiteTurn);
        if(isDraw)
            setMessage("Draw!");
        else
            setMessage( isWhiteTurn ? "Black wins!" : "White wins");

        isEnd = true;
        return true;

    } // check draw first then the others ?


    public ChessBoard clone() {
        try {
            ChessBoard cloned = (ChessBoard) super.clone();
            cloned.board = new piece[8][8];
            for(int i=0; i<8; i++)
                for(int j=0; j<8; j++)
                    cloned.board[i][j] = board[i][j];
            if(enPassantable != null)
                cloned.enPassantable = enPassantable.clone();
            return cloned;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public boolean getTurn() {
        return isWhiteTurn;
    }

    /**
     * Checks if there is an enPassant case so that I can capture.
     */
    public boolean isEnPassant(int x0, int y0) {
        if(enPassantable!=null && enPassantColor!=isWhiteTurn) {
            return x0 == enPassantable[0] && y0 == enPassantable[1];
        }
        return false;
    }

    /**
     * Sets the enPassant state. This method is called by Pawn that makes double state move.
     */
    public void setEnPassant(int x0, int y0) {
        enPassantColor = isWhiteTurn;
        enPassantable = new int [] {x0, y0};
    }

    /**
     * This method provides a mechanism for resetting EnPassant state, so that if the opponent does
     * not take advantage of this rule immediately, his rights to do so is discarded anymore. Functionsresets the
     * state by checking who-which player- is calling the function, because only after I make 2nd move, the state is lsot; not before
     * my opponent makes, because he can use this move ability.
     */
    public void resetEnPassant() {
        if(enPassantColor == isWhiteTurn) {
            enPassantable = null;
        }
    }

    public void pop() {

        board = restorePoint.board;

        isWhiteCastlingRight = restorePoint.isWhiteCastlingRight;
        isWhiteCastlingLeft = restorePoint.isWhiteCastlingLeft;
        isBlackCastlingRight = restorePoint.isBlackCastlingRight;
        isBlackCastlingLeft = restorePoint.isBlackCastlingLeft;

        isDraw = restorePoint.isDraw;
        // message = restorePoint.message;

        isWhiteTurn = restorePoint.isWhiteTurn;

        enPassantable = restorePoint.enPassantable;
        enPassantColor = restorePoint.enPassantColor;

        real = restorePoint.real;
        restorePoint = restorePoint.restorePoint;
    }

    public void setMessage(String s){
        message.setText(s);
    }

    public JLabel getMessage() {
        return message;
    }

    public int [] allMoves(piece p) {

        ArrayList<Integer> allMoves = new ArrayList<Integer>();
        int [] location = getLocation(p);

        for(int i=0; i<8; i++)
            for(int j=0; j<8; j++)
                if(checkMove(new int [] {location[0], location[1], i, j}, p)) {
                    allMoves.add(i);
                    allMoves.add(j);
                }

        if(allMoves.size() == 0)
            return null;
        else
        {
            int [] temp = new int[allMoves.size()];
            for(int i=0; i<temp.length; i++)
                temp[i] = allMoves.get(i);
            return temp;
        }
    }

    public JLabel getWhiteTimer() {
        return whiteWatch.getLabel();
    }

    public JLabel getBlackTimer() {
        return blackWatch.getLabel();
    }

    public boolean isEnd() {

        return isEnd;
    }

    public void restart() {

        blackWatch = new Watch();
        whiteWatch = new Watch();
        whiteWatch.startStop();

        board = new piece[8][8];

        // Sequences of the pieces for the beginning of the game
        for(int j=0; j<8; j++) {

            // Prepare row=0 and row=7 in here
            if(j==0 || j==7) {
                boolean color = j==0; // color of the pieces
                board[0][j] = new Rook(this, color); // construct with color
                board[1][j] = new Knight(this, color);
                board[2][j] = new Bishop(this, color);
                board[3][j] = new Queen(this, color);
                board[4][j] = new King(this, color);
                board[5][j] = new Bishop(this, color);
                board[6][j] = new Knight(this, color);
                board[7][j] = new Rook(this, color);

            } else if(j==1 || j==6) {
                boolean color = j==1;
                for(int i=0; i<8; i++) {
                    board[i][j] = new Pawn(this, color);
                }
            } else
                for(int i=0; i<8; i++)
                    board[i][j] = new Empty(this, false);
        }

        isWhiteCastlingLeft = true;
        isWhiteCastlingRight = true;
        isBlackCastlingLeft = true;
        isBlackCastlingRight = true;

        real = true;
        isDraw = false;
        isEnd = false;

        isWhiteTurn = true;

        //TODO restart everytime has done enPassant
        enPassantable = null;

        message = new JLabel();
        new Thread() {
            @Override
            public void run() {
                while(!whiteWatch.isEnd() && !blackWatch.isEnd()) {
                    if( whiteWatch.update() || blackWatch.update()) {
                        setChanged();
                        notifyObservers();
                    }
                }
                if(whiteWatch.isEnd())
                    message.setText("Black wins!");
                if(blackWatch.isEnd())
                    message.setText("White wins!");
                isEnd = true;
                whiteWatch.stop();
                blackWatch.stop();
            }
        }.start();

    }

    public void stopTimers() {

        whiteWatch.offTimer();
        blackWatch.offTimer();
    }
}