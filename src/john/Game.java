package john;

import john.gui.BoardDrawer;

/**
 * Created by john on 7/10/14.
 */
public class Game {

    public static ChessBoard gameBoard;
    public static BoardDrawer gui;

    public static void main(String [] args) {

        gameBoard = new ChessBoard();
        gui = new BoardDrawer(gameBoard);
        gameBoard.addObserver(gui);
    }

    public static void gameStart() {

        gameBoard.restart();
        gui.restart();
    }
}
